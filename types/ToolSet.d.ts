export default ToolSet;
export type TSSchema = {
    /**
     * - Type of the object
     */
    type: string;
    /**
     * - Description of properties
     */
    properties: object;
    /**
     * - Required properties
     */
    required?: string[];
};
export type TSTool = {
    /**
     * - Command description
     */
    description: string;
    /**
     * - JS JSON schema
     */
    parameters: TSSchema;
    method: (arg0: object) => Promise<any>;
};
export type TSToolListItem = {
    /**
     * - Command name
     */
    name: string;
    /**
     * - Command description
     */
    description: string;
    /**
     * - JS JSON schema
     */
    parameters: TSSchema;
};
export type PRRecord = {
    /**
     * - remote request id or generated requestId
     */
    id: string;
    /**
     * - Date in ISO string format
     */
    isoDate: string;
    /**
     * - Execution time in MS of the model / method
     */
    duration: number;
    /**
     * - Execution context endpoint_name / server_name / interpreter_name
     */
    environment: string;
    /**
     * - LLM model, method , funtion name
     */
    model: string;
    /**
     * - Number of tokens to process
     */
    tokensIn: number;
    /**
     * - Number of tokens in the response
     */
    tokensOut: number;
};
export type TSCallResponse = {
    response: any;
    /**
     * - a list of performed actions for billing and optimizing purposes.
     */
    records: PRRecord[];
};
declare class ToolSet {
    /**
    * @param {string} [choice] - Default 'auto' auto|none|required
    */
    constructor(choice?: string);
    /**
    * How many functions have we registered
    * @returns {number}
    */
    get length(): number;
    /**
    * Register a Tool/Command/Function
    * @param {string} name - [a-z_0-9]{2,} The lowercase string name of the callback e.g. 'get_node_version'
    * @param {string} description - What does it do
    * @param {TSSchema} parameters - SCHEMA object describing the function's input parameters
    * @param {function(object): Promise<TSCallResponse>} method - Async function to call
    */
    add(name: string, description: string, parameters: TSSchema, method: (arg0: object) => Promise<TSCallResponse>): void;
    /**
    * Is 'name' already registered
    * @param {string} name
    * @returns {boolean}
    */
    has(name: string): boolean;
    /**
    * Get a list of tools available
    * @returns {TSToolListItem[]}
    */
    list(): TSToolListItem[];
    get toolChoice(): string;
    /**
    * Execute a method
    * @param {string} name
    * @param {object} params
    * @returns {Promise<TSCallResponse>} Whatever the output is
    */
    call(name: string, params: object): Promise<TSCallResponse>;
    #private;
}
