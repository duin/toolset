#!/usr/bin/env node
import assert from 'node:assert/strict';
import ToolSet from '../lib/ToolSet.js'
/**
* Create a async context in an sync block
* @param {function} callback - async function
* @example
* const p = within(async () => {
*		const res = await Promise.all([
*			SH`sleep 1; echo 1`.run(),
*			SH`sleep 2; echo 2`.run(),
*			sleep(2),
*			SH`sleep 3; echo 3`.run()
*		]);
*/
const within = (callback) => {
	(async () => {
		return await callback()
	})()
}


within(async () => {
	const tools = new ToolSet('auto');
	const parameters = {
		type: 'object',
		properties: {
			location: {
				type: 'string',
				description: 'The city and state, e.g. San Francisco, CA'
			},
			unit: {
				type: 'string',
				enum: ["celsius", "fahrenheit"]
			}
		},
		required: ["location"]
	}
	tools.add(
		'get_current_weather', // name
		'Get the current weather in a given location', // desciption
		parameters,
		async (params) => {
			const records = [];
			const response = {
				location: params.location,
				temperature: 24,
				unit: params.unit
			}
			return { records, response };
		}
	);
	assert.equal(tools.has('get_current_weather'), true);
	assert.equal(tools.has('i_am_not_here'), false);
	const ob = await tools.call('get_current_weather', { location: 'Den haag' })
	assert.equal(ob.response.temperature, 24)
	const list = tools.list();
	assert.equal(list.length, 1);
	assert.equal(list[0].name, 'get_current_weather');;
});

within(async () => {
	const tools = new ToolSet('auto');
	const parameters = {
		type: 'object',
		properties: {
			location: {
				type: 'string',
				description: 'The city and state, e.g. San Francisco, CA'
			},
			unit: {
				type: 'string',
				enum: ["celsius", "fahrenheit"]
			}
		},
		required: ["location"]
	}
	tools.add(
		'#bash!', // name
		'Get the current weather in a given location', // desciption
		parameters,
		async (params) => {
			const records = [];
			const response = {
				location: params.location,
				temperature: 24,
				unit: params.unit
			}
			return { records, response };
		}
	);
	assert.equal(tools.has('#bash!'), true);
	assert.equal(tools.has('i_am_not_here'), false);
	const ob = await tools.call('#bash!', { location: 'Den haag' })
	assert.equal(ob.response.temperature, 24)
	const list = tools.list();
	assert.equal(list.length, 1);
	assert.equal(list[0].name, '#bash!');;
});

