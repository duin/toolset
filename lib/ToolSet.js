/**
* @module @j-o-r/toolset
* Register function_calls / tools
* This library can be used stand-alone or as a wrapper for AI functions_calls tools
*/

/**
* @typedef {object} TSSchema
* @property {string} type - Type of the object
* @property {object} properties - Description of properties
* @property {string[]} [required] - Required properties
*/

/**
* @typedef {object} TSTool
* @property {string} description - Command description
* @property {TSSchema} parameters - JS JSON schema
* @property {function(object): Promise} method
*/

/**
* @typedef {object} TSToolListItem
* @property {string} name - Command name
* @property {string} description - Command description
* @property {TSSchema} parameters - JS JSON schema
*/

/**
* @typedef {Object} PRRecord
* @property {string} id - remote request id or generated requestId
* @property {string} isoDate - Date in ISO string format
* @property {number} duration - Execution time in MS of the model / method
* @property {string} environment - Execution context endpoint_name / server_name / interpreter_name
* @property {string} model - LLM model, method , funtion name
* @property {number} tokensIn - Number of tokens to process
* @property {number} tokensOut - Number of tokens in the response
*/

/**
* @typedef {object} TSCallResponse
* @property {any} response
* @property {PRRecord[]} records - a list of performed actions for billing and optimizing purposes.
*/

/** Type of choices */
const CHOICES = {
	NONE: 'none',
	AUTO: 'auto',
	REQUIRED: 'required'
};

/**
* The function name is limited
* @param {string} s
* @returns {boolean}
*/
const isValidName = (s) => /^[#!a-z_0-9]{2,}$/.test(s);

class ToolSet {
	/** @type {Map<string, TSTool>} */
	#tools = new Map();
	/** default auto */
	#toolChoice = 'auto';

	/**
	* @param {string} [choice] - Default 'auto' auto|none|required
	*/
	constructor(choice = 'auto') {
		if (choice && Object.values(CHOICES).includes(choice)) {
			this.#toolChoice = choice;
		} else if (choice) {
			throw new Error('Tool choice not defined');
		}
	}

	/**
	* How many functions have we registered
	* @returns {number}
	*/
	get length() {
		return this.#tools.size;
	}

	/**
	* Register a Tool/Command/Function
	* @param {string} name - [a-z_0-9]{2,} The lowercase string name of the callback e.g. 'get_node_version'
	* @param {string} description - What does it do
	* @param {TSSchema} parameters - SCHEMA object describing the function's input parameters
	* @param {function(object): Promise<TSCallResponse>} method - Async function to call
	*/
	add(name, description, parameters, method) {
		if (!isValidName(name)) {
			throw new Error('Invalid name /[a-z_0-9]{2,}/');
		}
		if (this.has(name)) {
			throw new Error('Function already defined');
		}
		this.#tools.set(name, { description, parameters, method });
	}

	/**
	* Is 'name' already registered
	* @param {string} name
	* @returns {boolean}
	*/
	has(name) {
		return this.#tools.has(name);
	}

	/**
	* Get a list of tools available
	* @returns {TSToolListItem[]}
	*/
	list() {
		return Array.from(this.#tools.entries()).map(([name, value]) => ({
			name,
			description: value.description,
			parameters: value.parameters
		})).sort((a, b) => a.name.localeCompare(b.name));
	}

	get toolChoice() {
		return this.#toolChoice;
	}
	/**
	* Execute a method
	* @param {string} name
	* @param {object} params
	* @returns {Promise<TSCallResponse>} Whatever the output is
	*/
	async call(name, params) {
		if (!this.has(name)) {
			throw new Error('Function not found');
		}
		const tool = this.#tools.get(name);
		return tool.method(params);
	}
}

export default ToolSet;
