# ToolSet

A simple JavaScript library for registering and managing function calls/tools. This library can be used stand-alone or in combination with AI functions/calls tools.

## Table of Contents

- [Features](#features)
- [Installation](#installation)
- [Usage](#usage)
- [API](#api)

## Features

- Register tools/commands/functions with descriptions and parameters.
- Validate tool names.
- List all registered tools.
- Execute registered tools asynchronously.
- Configurable tool choice options (`auto`, `none`, `required`).

## Installation

To install the ToolSet library, you can use npm:

```bash
npm install @j-o-r/toolset --save
```

## Usage

Here is a basic example of how to use the ToolSet library:

```javascript
import ToolSet from 'toolset';

const tools = new ToolSet('auto');

const parameters = {
  type: 'object',
  properties: {
    location: {
      type: 'string',
      description: 'The city and state, e.g. San Francisco, CA'
    },
    unit: {
      type: 'string',
      enum: ["celsius", "fahrenheit"]
    }
  },
  required: ["location"]
};

tools.add(
  'get_current_weather',
  'Get the current weather in a given location',
  parameters,
  async (params) => {
    const res = {
      location: params.location,
      temperature: 24,
      unit: params.unit
    };
    return res;
  }
);

const weather = await tools.call('get_current_weather', { location: 'Den Haag' });
console.log(weather); // { location: 'Den Haag', temperature: 24, unit: undefined }
```

## API

### ToolSet

#### `constructor(choice)`

- `choice` (optional): A string that can be `auto`, `none`, or `required`. Default is `auto`.

#### `add(name, description, parameters, method)`

- `name`: The name of the tool.
- `description`: A brief description of what the tool does.
- `parameters`: A schema object describing the tool's input parameters.
- `method`: An asynchronous function to be called when the tool is executed.

#### `has(name)`

- `name`: The name of the tool.
- Returns `true` if the tool is registered, otherwise `false`.

#### `list()`

- Returns a list of all registered tools.

#### `call(name, params)`

- `name`: The name of the tool to be executed.
- `params`: An object containing the parameters for the tool.
- Returns a promise that resolves with the tool's output.

## License

This project is licensed under the APACHE 2.0 License. See the LICENSE file for details.
